Recipe Finder - README
****************************

This is the solution to the Recipe Finder Technical challenge

1) Project Structure

/src
	This folder contains the source code to solve the Mars Rover challenge which has been divided in four files as follows:

		1. Cook.php - This class represents the cooking posibilities of recipes according to the ingredients stored in the fridge
		2. Fridge.php   - This class represents a fridge to store ingredients
		3. Ingredient.php   - This class represents an Ingredient

/test
	This folder contains the test cases for the main classes of the project which are Position and the Plateau classes

		1. CookTest.php  - Tests the cook functionality
		2. FridgeTest.php	- Tests the fridge funtionality and its main methods

/docs
    It has a document with the problem description (see RecipeFinderProblem.pdf)


2) System Requirements
	1. PHP7
	2. PHP-Unit

3) How to run the program
	1. Go inside de main folder (RecipeFinder)
	2. run the following command from your teminal: php src/RecipeFinder.php src/fridge.csv src/recipes.json 
	3. Please notice that the fridge.csv and recipes.json are just inout examples and they can be modified as neeed it


3) How to run the unit tests
    1. Go inside de main folder (RecipeFinder)
	2. run the following command from your teminal:
	    2.1. phpunit --bootstrap src/Fridge.php test/FridgeTest.php 
        2.2. phpunit --bootstrap src/Cook.php test/CookTest.php 