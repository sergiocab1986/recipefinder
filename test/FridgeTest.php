<?php
declare(strict_types=1);

require_once "src/Fridge.php";
require_once "src/Ingredient.php";
use PHPUnit\Framework\TestCase;

/**
 * @covers Fridge
 */
final class FridgeTest extends TestCase
{

    public function testCheckIngredientExistValid()
    {
        $ingredientBread = ['bread', '10', 'slices', '25/12/2017'];
        $ingredientButter = ['butter', '800', 'grams', '28/12/2017'];
        $fridge = new Fridge();
        $fridge->addIngredient($ingredientBread);
        $fridge->addIngredient($ingredientButter);
        $ingredientBread = new Ingredient('bread', '2', 'slices', '23/12/2017');
        $ingredientButter = new Ingredient('butter', '200', 'grams', '20/12/2017');

        $this->assertNotNull($fridge->checkIngredientExist($ingredientBread));
        $this->assertNotNull($fridge->checkIngredientExist($ingredientButter));
    }

    public function testCheckIngredientDoesNotExist()
    {
        $ingredientBread = ['bread', '10', 'slices', '25/12/2017'];
        $ingredientButter = ['butter', '800', 'grams', '28/12/2017'];
        $fridge = new Fridge();
        $fridge->addIngredient($ingredientBread);
        $fridge->addIngredient($ingredientButter);
        $ingredientBread = new Ingredient('bread', '2', 'slices', '23/12/2017');
        $ingredientButterAlot = new Ingredient('butter', '900', 'grams', '20/12/2017');
        $ingredientEggs = new Ingredient('eggs', '2', 'of', '20/12/2017');

        $this->assertNull($fridge->checkIngredientExist($ingredientEggs));
        $this->assertNull($fridge->checkIngredientExist($ingredientButterAlot));
    }


    public function testCheckIngredientExpired()
    {
        $ingredientBread = ['bread', '10', 'slices', '25/12/2015'];
        $ingredientButter = ['butter', '800', 'grams', '10/12/2017'];
        $fridge = new Fridge();
        $fridge->addIngredient($ingredientBread);
        $fridge->addIngredient($ingredientButter);
        $ingredientBread = new Ingredient('bread', '2', 'slices', '23/12/2017');
        $ingredientButterAlot = new Ingredient('butter', '900', 'grams', '20/12/2017');
        $ingredientEggs = new Ingredient('eggs', '2', 'of', '20/12/2017');

        $this->assertNull($fridge->checkIngredientExist($ingredientEggs));
        $this->assertNull($fridge->checkIngredientExist($ingredientButterAlot));
    }

    public function testCanCookRecipeValid()
    {
        $ingredientBread = ['bread', '10', 'slices', '25/12/2017'];
        $ingredientButter = ['butter', '800', 'grams', '10/12/2017'];
        $fridge = new Fridge();
        $fridge->addIngredient($ingredientBread);
        $fridge->addIngredient($ingredientButter);
        $recipe['ingredients'] = array( array('item' => 'bread', 'amount' => '2', 'unit'=>'slices'),
                                        array('item' => 'butter', 'amount' => '100', 'unit'=>'grams')
                                    );

        $this->assertNotNull($fridge->checkCanCookRecipe($recipe));
    }

    public function testCanNotCookRecipeMissingItem()
    {
        $ingredientBread = ['bread', '10', 'slices', '25/12/2017'];
        $ingredientButter = ['butter', '800', 'grams', '17/12/2017'];
        $fridge = new Fridge();
        $fridge->addIngredient($ingredientBread);
        $fridge->addIngredient($ingredientButter);
        $recipe['ingredients'] = array( array('item' => 'bread', 'amount' => '2', 'unit'=>'slices'),
                                        array('item' => 'butter', 'amount' => '100', 'unit'=>'grams'),
                                        array('item' => 'eggs', 'amount' => '1', 'unit'=>'of')
                                    );

        $this->assertNull($fridge->checkCanCookRecipe($recipe));
    }

    public function testCanNotCookRecipeNotEnough()
    {
        $ingredientBread = ['bread', '10', 'slices', '25/12/2017'];
        $ingredientButter = ['butter', '800', 'grams', '10/12/2017'];
        $ingredientButter = ['eggs', '3', 'of', '17/12/2017'];
        $fridge = new Fridge();
        $fridge->addIngredient($ingredientBread);
        $fridge->addIngredient($ingredientButter);
        $recipe['ingredients'] = array( array('item' => 'bread', 'amount' => '2', 'unit'=>'slices'),
                                        array('item' => 'butter', 'amount' => '100', 'unit'=>'grams'),
                                        array('item' => 'eggs', 'amount' => '6', 'unit'=>'of')
                                    );

        $this->assertNull($fridge->checkCanCookRecipe($recipe));
    }

}