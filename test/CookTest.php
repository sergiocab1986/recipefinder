<?php
declare(strict_types=1);

require_once "src/Cook.php";
require_once "src/Fridge.php";
require_once "src/Ingredient.php";
use PHPUnit\Framework\TestCase;

/**
 * @covers Cook
 */
final class CookTest extends TestCase
{

    public function testGetRecommendedRecipeValid()
    {
        $expectedRecipe = "bread with butter";
        $ingredientBread = ['bread', '10', 'slices', '25/12/2017'];
        $ingredientButter = ['butter', '800', 'grams', '28/12/2017'];
        $ingredientEggs = ['eggs', '3', 'of', '17/12/2017'];
        $cook = new Cook();
        $cook->addIngredient($ingredientBread);
        $cook->addIngredient($ingredientButter);
        $cook->addIngredient($ingredientEggs);
        $cook->addRecipe( array(  "name" => $expectedRecipe,
                                    "ingredients" => array( array('item' => 'bread', 'amount' => '2', 'unit'=>'slices'),
                                                            array('item' => 'butter', 'amount' => '100', 'unit'=>'grams')
                                                         )
                                )
                        );

        $recommendedRecipe = $cook->getRecommendedRecipe();              
        $this->assertNotNull($recommendedRecipe);
        $this->assertEquals($expectedRecipe, $recommendedRecipe);
    }

    public function testGetRecommendedRecipeNotValid()
    {
        $expectedRecipe = "Order Takeout";
        $ingredientBread = ['bread', '10', 'slices', '25/12/2017'];
        $ingredientButter = ['butter', '800', 'grams', '28/12/2017'];
        $ingredientEggs = ['eggs', '3', 'of', '17/12/2017'];
        $cook = new Cook();
        $cook->addIngredient($ingredientBread);
        $cook->addIngredient($ingredientButter);
        $cook->addIngredient($ingredientEggs);
        $cook->addRecipe( array(  "name" => "bread with cheese",
                                    "ingredients" => array( array('item' => 'bread', 'amount' => '2', 'unit'=>'slices'),
                                                            array('item' => 'cheese', 'amount' => '1', 'unit'=>'slice')
                                                         )
                                )
                        );

        $recommendedRecipe = $cook->getRecommendedRecipe();              
        $this->assertNotNull($recommendedRecipe);
        $this->assertEquals($expectedRecipe, $recommendedRecipe);
    }

    public function testGetRecommendedRecipeNotEnough()
    {
        $expectedRecipe = "Order Takeout";
        $ingredientBread = ['bread', '10', 'slices', '25/12/2017'];
        $ingredientButter = ['butter', '800', 'grams', '28/12/2017'];
        $ingredientEggs = ['eggs', '3', 'of', '17/12/2017'];
        $cook = new Cook();
        $cook->addIngredient($ingredientBread);
        $cook->addIngredient($ingredientButter);
        $cook->addIngredient($ingredientEggs);
        $cook->addRecipe( array(  "name" => "bread with cheese",
                                    "ingredients" => array( array('item' => 'bread', 'amount' => '2', 'unit'=>'slices'),
                                                            array('item' => 'eggs', 'amount' => '5', 'unit'=>'of')
                                                         )
                                )
                        );

        $recommendedRecipe = $cook->getRecommendedRecipe();              
        $this->assertNotNull($recommendedRecipe);
        $this->assertEquals($expectedRecipe, $recommendedRecipe);
    }
}
?>