<?php

/**
* This class represents an ingredient.
* @author     Sergio Cabrera
*/
class Ingredient
{
	/**
     * This name of the ingredient
     * @var string
     */
	var $item;

	/**
     * This the amount of the ingredient
     * @var string
     */
	var $amount;

	/**
     * This name the unit of measurement of the ingredient (e.g. slices, grams, ml) 
     * @var string
     */
	var $unit;

	/**
     * This is the use by date of the ingredient (dd/mm/yyyy)
     * @var string
     */
	var $useby;

	public function __construct($item, $amount, $unit, $useby)
    {
        $this->item = $item;
        $this->amount = $amount;
        $this->unit = $unit;
    	$this->useby = $useby;
    }
}
?>