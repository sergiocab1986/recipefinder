<?php

/**
 * This class represents the cooking posibilities of recipes according to the ingredients stored in the fridge
 * @author  Sergio Cabrera
*/
class Cook
{
    /**
     * This is the fridge that stores the ingredients
     * @var Fridge
     */
    var $fridge;

    /**
     * A list of recipes to be valuated with their ingredients
     * @var array of recipes
     */
    var $recipes = [];

    /**
     * This is the closest expiry day of the potential recipe
     * @var date 
     */
    var $closesExpiry = null;

    /**
     * This is the name of the potential recipe to be cook
     * @var string 
     */
    var $recommendedRecipe = 'Order Takeout';

    /**
     * It creates a nw Fridge to store the ingredients
     */
    public function __construct()
    {
        $this->fridge = new Fridge();
    }

    /**
     * Adds ingredients to the fridge
     */
    function addIngredient($ingredient) {
        $this->fridge->addIngredient($ingredient);
    }

    /**
     * Adds potential recipes to the list
     */
    function addRecipe($recipe) {
        array_push($this->recipes, $recipe);
    }

    /**
     * Gets the recommended recipe name according to the ingredients available in the fridge and their expiry date 
     */
    function getRecommendedRecipe() {
        foreach ($this->recipes as $recipe) {
            $canCookDate = $this->fridge->checkCanCookRecipe($recipe);
                if ($canCookDate != null) {
                    if ($this->closesExpiry == null || strtotime(str_replace('/', '-', $this->closesExpiry)) > $canCookDate) {
                                    $this->closesExpiry = $canCookDate;
                                    $this->recommendedRecipe = $recipe['name'];
                            }
                    }
        }
        return $this->recommendedRecipe;
    }
}
?>