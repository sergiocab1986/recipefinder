<?php
require ('Cook.php');
require ('Fridge.php');
require ('Ingredient.php');

$cook = new Cook();

if (count($argv) != 3) {
	echo 'Invalid Input Parameters';
}
else {
	// Read ingredients in the fridge
	$file = fopen($argv[1], 'r');
	while (($line = fgetcsv($file)) !== FALSE) {
		$cook->addIngredient($line);
	}
	fclose($file);
	
	// Read the recipes from the json file
  $string = file_get_contents($argv[2]);
	$jsonArray = json_decode($string, true);

	foreach ($jsonArray as $recipeDetails) {
			$cook->addRecipe($recipeDetails);
  }
  echo $cook->getRecommendedRecipe();
}
//$fridge->printFridge();
?>