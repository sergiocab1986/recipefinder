<?php

/**
 * This class represents a fridge to store ingredients
 * @author  Sergio Cabrera
*/
class Fridge
{
    /**
     * Represents the list of ingredients currently available in the fridge
     */
	var $ingredients = [];

    /**
     * Adds ingredients to the fridge
     */
    function addIngredient($ingredientDetails) {
        $ingredient = new Ingredient($ingredientDetails[0], $ingredientDetails[1], $ingredientDetails[2], $ingredientDetails[3]);
        array_push($this->ingredients, $ingredient);
    }

    /**
     * Prints the current ingredients in the fridge
     */
    function printFridge() {
        var_dump($this->ingredients);
    }

    /**
     * Checks if the recipe given can be cook or not based on the ingredients available in the fridge
     * @param array An array with the recipe ingredients to required for the recipe
     * @return date Returns the closest expiry date of the ingredients in the recipe, or null if one of the ingredients is not available
     */
    function checkCanCookRecipe($recipe) {
        $closesExpiry = null;
        $today = new DateTime('');
        $todayStr = $today->format('d/m/Y');

        foreach ($recipe['ingredients'] as $ingredientArray) {
            $ingredient = new Ingredient($ingredientArray['item'], $ingredientArray['amount'], $ingredientArray['unit'], $todayStr);
            $ingredientFound = $this->checkIngredientExist($ingredient);
            if ($ingredientFound == null) {
                return null;
            }

            $fridgeExpDate = strtotime(str_replace('/', '-', $ingredientFound->useby));
            if ($closesExpiry == null || strtotime(str_replace('/', '-', $closesExpiry)) > $fridgeExpDate) {
                $closesExpiry = $ingredientFound->useby;
            }
        }
        return $closesExpiry;
    }

    /**
     * Checks if an specific ingreident is available in the fridge
     * @param Ingredient Is the ingredient that needs to be checked
     * @return date Returns the expiry date of the ingredient checked, or null the ingredient is not available
     */
    function checkIngredientExist($ingredient) { 
        foreach ($this->ingredients as $frigeIngredient) {
            $fridgeExpDate = strtotime(str_replace('/', '-', $frigeIngredient->useby));
            $ingredientExpdate = strtotime(str_replace('/', '-',$ingredient->useby));
            if ($frigeIngredient->item == $ingredient->item && $frigeIngredient->amount >= $ingredient->amount &&
                $frigeIngredient->unit == $ingredient->unit && $fridgeExpDate >= $ingredientExpdate) {
                return $frigeIngredient;
            }
        }
        return null;
    }

}
?>